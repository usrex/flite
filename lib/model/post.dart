class Post {
  int id;
  String rutaImg;
  String titulo;
  String autor;
  int estado; //0 no esta en la db, 1 esta en la db

  Post({
    this.id,
    this.rutaImg,
    this.titulo = 'lorem impsun lorem impsun lorem impsun lorem impsun',
    this.autor = 'by UsRex',
    this.estado = 0,
  });

  Post.fromMap(Map<String, dynamic> data){
    this.id = data['id'];
    this.rutaImg = data['imagen'];
    this.titulo = data['titulo'];
    this.autor = data['autor'];
    this.estado = data['estado'];
  }

  Map<String, dynamic> toMap(){
    return {
      'id': id,
      'imagen': rutaImg,
      'titulo': titulo,
      'autor': autor,
      'estado': estado,
    };
  }

  @override
  String toString() {
    return 'Post id $id, img $rutaImg, titulo $titulo, autor $autor, estado $estado';
  }
}
