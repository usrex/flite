import 'package:flutter/material.dart';

class LogInPage extends StatefulWidget {
  @override
  _LogInPageState createState() => _LogInPageState();
}

class _LogInPageState extends State<LogInPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _login(),
    );
  }

  Widget _login() {
    return SafeArea(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 45),
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.only(top: MediaQuery.of(context).size.width/3) ,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                ClipOval(
                  child: FlutterLogo(
                    colors: Colors.purple,
                    size: 200,
                  ),
                ),
                SizedBox(
                  height: 125,
                ),
                TextField(
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(labelText: "USERNAME"),
                ),
                SizedBox(
                  height: 25,
                ),
                TextField(
                  decoration: InputDecoration(labelText: "PASSWORD"),
                  keyboardType: TextInputType.visiblePassword,
                  obscureText: true,
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[Text("FORGOT PASSWORD")],
                  ),
                ),
                SizedBox(
                  height: 36,
                ),
                Container(
                  child: InkWell(
                    onTap: () {
                      Navigator.of(context).pushNamed("/Home");
                      print(MediaQuery.of(context).size.width);
                    },
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                      decoration: BoxDecoration(
                        color: Colors.grey,
                      ),
                      width: 250,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          Text("SIGN IN",
                              style: TextStyle(
                                color: Colors.white,
                              )),
                          SizedBox(
                            width: 35,
                          ),
                          Icon(
                            Icons.arrow_forward,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
