import 'package:clasesql/model/post.dart';
import 'package:sqflite/sqflite.dart';

class DbProvider {
  static const TABLE_NAME = 'posts';

  Future<Database> db = openDatabase(
    'my_db.db',
    version: 1,
    onCreate: (Database db, int version) async {
      await db.execute('''
        CREATE TABLE $TABLE_NAME(
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          imagen TEXT,
          titulo TEXT, 
          autor TEXT,
          estado INTEGER
        );
        ''');
    },
  );

  Future<int> insertarPost(Post post) async {
    Database db = await this.db;
    int resp = await db.insert(TABLE_NAME, post.toMap());
    print('********INSERT************');
    print(resp);
    return resp;
  }

  Future<List<Post>> obtenerPosts() async {
    Database db = await this.db;
    List<Map<String, dynamic>> resp = await db.query(TABLE_NAME);
    final lista = resp.map((Map<String, dynamic> data) {
      return Post.fromMap(data);
    }).toList();
    print('********OBTENER************');
    print(resp);
    return lista.isEmpty ? [] : lista;
  }

  Future<List<Post>> obtenerPostsFavoritos() async {
    Database db = await this.db;
    List<Map<String, dynamic>> resp = await db.query(
      TABLE_NAME,
      where: 'estado = ?',
      whereArgs: [1],
    );
    final lista = resp.map((Map<String, dynamic> data) {
      return Post.fromMap(data);
    }).toList();
    print('********OBTENER************');
    print(resp);
    return lista.isEmpty ? [] : lista;
  }

  Future borrarPorId(int id) async {
    Database db = await this.db;
    int resp = await db.delete(TABLE_NAME, where: 'id = ?', whereArgs: [id]);
    print('********DELETE************');
    print(resp);
  }

  Future actualizarPost(Post post) async {
    Database db = await this.db;
    int resp = await db.update(TABLE_NAME, post.toMap(),
        where: 'id = ?', whereArgs: [post.id]);
    print('********ACTUALIZAR************');
    print(resp);
  }
}
